provider "aws" {
}

provider "aws" {
  alias  = "india"
  region = local.india_region
}

provider "aws" {
  alias  = "us1"
  region = local.us1_region
}
